var gulp = require("gulp");
var browserSync = require('browser-sync').create();

gulp.task('serve', function(){
  browserSync.init({
    server: './site'
  });
  gulp.watch('site/*.htm*', ['serve']).on('change', browserSync.reload);
  gulp.watch('site/styles/*.css', ['serve']).on('change', browserSync.reload);
  gulp.watch('site/scripts/*.js*', ['serve']).on('change', browserSync.reload);
  gulp.watch('site/images/*', ['serve']).on('change', browserSync.reload);
});

gulp.task('default',['serve']);
