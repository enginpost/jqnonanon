var pageConfiguration, kpiTarget, pageCycleInterval;

(function($){
  $(document).ready(function(){

    kpiTarget = $("script[src*='main.js']").attr('data-kpi-target').indexOf("#") > -1 ? $("script[src*='main.js']").attr('data-kpi-target') : "#" + 
                $("script[src*='main.js']").attr('data-kpi-target');

    function loadThePageConfig(){
      THIS_PAGE.loadPageConfiguration('/data/data.json', function(data){
        pageConfiguration = data;
        buildThePage();
      });
    }

    function buildThePage(){
      for( var i = 0; i < pageConfiguration.KPIs.length; i++){
        var thisKPI = pageConfiguration.KPIs[i];
        var thisKPIDisplay = Object.create(KpiManager);
        thisKPIDisplay.buildKpiDisplay(
            pageConfiguration.templates[thisKPI.kpi_template],kpiTarget, 
            "kpi_" +i, 
            thisKPI.kpi_title, 
            thisKPI.kpi_data,
            "past-threshold");
        thisKPI.kpi_display = thisKPIDisplay;
      }
      refreshPageData();
    }

    function refreshPageData(){
      if( pageCycleInterval ){
        clearInterval(pageCycleInterval);
      }
      for( var i = 0; i < pageConfiguration.KPIs.length; i++){
        $.ajax({
          url: pageConfiguration.KPIs[i].kpi_display.kpiData,
          displayIndex: i,
          success: function(data){
            pageConfiguration.KPIs[this.displayIndex].kpi_display.setKpi(data.result);
          }
        });
      }
      pageCycleInterval = setInterval(refreshPageData,60000);
    }

    //it all starts here
    loadThePageConfig();

  });
})(jQuery);