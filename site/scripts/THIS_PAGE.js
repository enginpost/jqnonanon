(function($){
  THIS_PAGE = new function(){
    this.loadPageConfiguration = function(thisPageConfig, thisCallback){
      $.get(thisPageConfig, function(data){
        thisCallback(data);
      }).fail(function(err){
          console.log("Could not load the page configuration");
      });
    }
  }
})(jQuery);