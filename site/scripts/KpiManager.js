(function($){
  KpiManager = new function(){
    this.id = '';
    this.kpiData = '';
    this.highlightClass = '';
    this.setKpi = function(kpiValue){
        var kpiThreshold = $("#"+this.id+" .kpi-title").attr("data-kpi-threshold") != null ? $("#"+this.id+" .kpi-title").attr("data-kpi-threshold") : null;
        if(kpiThreshold != null && kpiThreshold < kpiValue){
            $("#"+this.id+" .kpi-value").addClass(this.highlightClass);
        }else{
            $("#"+this.id+" .kpi-value").removeClass(this.highlightClass);
        }
        $("#"+this.id+" .kpi-value").text(kpiValue);
    };
    this.buildKpiDisplay = function(thisTemplate, thisTarget, thisID, thisTitle, thisData, thisHighlightClass){
        this.id = thisID;
        this.kpiData = thisData;
        this.highlightClass = thisHighlightClass;
        var thisDisplay = $(thisTemplate);
        thisDisplay.attr("id",thisID);
        thisDisplay.find(".kpi-title").text(thisTitle);
        thisDisplay.find(".kpi-title").on("click",this.setThreshold);
        $(thisTarget).append(thisDisplay);
        //load threshold from cookies
        var threshold = Cookies.get(thisID);
        if(threshold != null) $("#"+thisID+" .kpi-title").attr("data-kpi-threshold",threshold);
    }
    this.setThreshold = function(){
        var value = prompt("Enter the numeric threshold");
        if( value != null ){
            $(this).attr("data-kpi-threshold",value);
            Cookies.set( $(this).parent().attr("id"),value);
        }
    }
    return this;
  }
})(jQuery);